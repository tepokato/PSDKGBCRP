# ![Pokémon GBC Resource Pack](https://pbs.twimg.com/media/EEYh-HzW4AAiryF?format=png)

## What is it ?

Pokémon GBC Resource Pack or `PSDKGBCRP` is a resource pack that allow to develop Pokémon Game in a GBC Style with Pokémon SDK. It provide various UI that makes your game looks like a GBC Game.

### Screenshots
![Screens](https://i.imgur.com/SpVpqAK.png)

## How to use it ?

Currently, you can simply download this git as zip and extract it in your project. It's supposed to be working correctly if your PSDK version is up to date (Alpha 24.38 / 25 minimum).

## How to develop on it ?
First of all you'll clone the git using the following command : 
```sh
git clone --recursive git@gitlab.com:tepokato/PSDKGBCRP.git
```
This will clone the Git Repository with all the submodules (including Pokémon SDK).

If you're using Windows, you'll execute `PSDKGBCRP.exe` and extract the files in the `PSDKGBCRP` folder created by the `git clone` command and ignore the files that already exists. (You should see a lib folder, Game.exe and a bunch of files that aren't tracked in the `PSDKGBCRP` folder once done).

If you're using Ubuntu or Linux, you should follow instruction given at the following link : [How to compile PSDK on Linux](https://pokemonworkshop.fr/en/2019/07/20/build-psdk-under-linux/#post-305)

Once done, you'll be able to edit the scripts inside `scripts/00001 PSDKGBCRP/`. Make sure you never edit the legacy scripts inside `pokemonsdk/scripts/` (see [CONTRIBUTING.md](./CONTRIBUTING.md) for more details).